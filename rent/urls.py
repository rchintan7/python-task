from django.contrib import admin
from django.urls import path, include
from car.views import *
from rest_framework_simplejwt.views import TokenRefreshView
import car.views as views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    
    path('api/api-auth/', include('rest_framework.urls')),
    path('api/login/', views.MyObtainTokenPairView.as_view(), name='token_obtain_pair'),
    path('api/login/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/register/', views.RegisterView.as_view(), name='auth_register'),
    path('api/user/<int:pk>/',views.UserAPI.as_view(),name='user'),
    
    path('api/car/',views.CarAPI.as_view(),name='car'),
    path('api/car/<int:pk>/',views.CarAPI.as_view(),name='car'),
    path('api/address/',views.UserAddressAPI.as_view(),name='address'),
    path('api/address/<int:pk>/',views.UserAddressAPI.as_view(),name='address'),
    path('api/ads/',views.AdAPI.as_view(),name='ads'),
    path('api/ads/<int:pk>/',views.AdAPI.as_view(),name='ads'),
]
static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)