# Generated by Django 4.1.3 on 2022-11-08 12:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('car', '0008_ad_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to=''),
        ),
    ]
