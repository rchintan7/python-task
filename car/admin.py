from django.contrib import admin
import car.models as models
# Register your models here.
admin.site.register(models.UserAddress)
admin.site.register(models.Car)
admin.site.register(models.Ad)
