from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import *
from rest_framework.authentication import * 
from rest_framework.permissions import *
from car.models import *
from car.serializers import *
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status, generics
from rest_framework.authentication import SessionAuthentication 

class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening

# This is User MyObtainTokenPair ViewAPI
class MyObtainTokenPairView(TokenObtainPairView):
    permission_classes = (AllowAny,)
    serializer_class = MyTokenObtainPairSerializer
    
# This is User Register ViewAPI
class RegisterView(generics.CreateAPIView):
    permission_classes = (AllowAny,)
    def post(self, request):
        username = request.POST.get('email')
        email = request.POST.get('email')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        password = request.POST.get('password')
        password2 = request.POST.get('password2')
        brand = request.POST.get('brand')
        model = request.POST.get('model')
        number_plate = request.POST.get('number_plate')
        print(username,email,first_name,last_name,password,password2,brand,model,number_plate)
        if "." not in email and "@" not in email:
            return Response({"success":False,'message':'Email is not valid!!'})
        if username and email and first_name and last_name and password and password2 and brand and model and number_plate:
            check_usr = User.objects.filter(email=email)
            if len(check_usr) > 0:
                return Response({"success":False,"message":"Email is already available.."})
            if password == password2:
                new_usr = User.objects.create(username=username,email=email,first_name=first_name,last_name=last_name)
                new_usr.set_password(password)
                new_usr.save()
                check_car = Car.objects.filter(user=new_usr)
                if len(check_car)>0:
                    check_car[0].brand = brand,model=model,number_plate=number_plate
                new_data = Car.objects.create(user=new_usr,brand = brand,model=model,number_plate=number_plate)
                return Response({
                'success':True,
                'message':"User registered successfully.."
                })
            else:
                return Response({"success":False,"message":"Password fields didn't match."})
        else:
            return Response({"success":False,"message":"Please fill all required fields.."})
        
# This is User Update ViewAPI
class UserAPI(APIView):
    premission_classes = (IsAuthenticated, )
    def get(self,request,pk,format=None):
        id=pk
        user1 = User.objects.get(id=id) 
        serializer = UserSerializer(user1) 
        return Response(({'success':True,'data':serializer.data}))
    def put(self,request,pk,format=None):
        id = pk
        user1 = User.objects.get(pk=id)   
        serializer = UserSerializer(user1, data=request.data) 
        if serializer.is_valid():
            serializer.save()
            return Response (({'success':True,'message':'Data Updated','data':serializer.data}))
        return Response (({'success':False,'message':'Something went wrong!!','data':serializer.errors}))

# This is User Address ViewAPI
class UserAddressAPI(APIView):
    premission_classes = (IsAuthenticated, )
    def get(self,request,pk=None,format=None):
        id = pk
        if id is not None:
            address = UserAddress.objects.get(id=id)
            serializer = UserAddressSerializer(address) 
            return Response(({'success':True,'data':serializer.data}))
        address = UserAddress.objects.filter(user=request.user) 
        serializer = UserAddressSerializer(address, many=True) 
        return Response(({'success':True,'data':serializer.data}))
    def post(self,request,format=None):
        address = UserAddress.objects.create(user=request.user)
        serializer = UserAddressSerializer(address, data=request.data) 
        if serializer.is_valid():
            serializer.save()  
            return Response (({'success':True,'message':'Data Created','data':serializer.data}), status= status.HTTP_201_CREATED)  
        return Response (({'success':False,'message':'Something went wrong!!','data':serializer.errors}))
    def put(self,request,pk,format=None):
        id = pk
        address = UserAddress.objects.get(pk=id,user=request.user)   
        serializer = UserAddressSerializer(address, data=request.data) 
        if serializer.is_valid():
            serializer.save()
            return Response (({'success':True,'message':'Data Updated','data':serializer.data}))
        return Response (({'success':False,'message':'Something went wrong!!','data':serializer.errors}))
    def delete(self, request, pk):
        id = pk
        address = UserAddress.objects.get(pk=id,user=request.user)   
        address.delete()
        return Response (({'success':True,'message':'Data Successfully Deleted'}))
    
 # This is User Car ViewAPI
class CarAPI(APIView):
    premission_classes = (IsAuthenticated, )
    def get(self,request,pk=None,format=None):
        id = pk
        if id is not None:
            usercar = Car.objects.get(id=id)
            serializer = CarSerializer(usercar) 
            return Response(({'success':True,'data':serializer.data}))
        usercar = Car.objects.filter(user=request.user) 
        serializer = CarSerializer(usercar, many=True) 
        return Response(({'success':True,'data':serializer.data}))
    def post(self,request):
        car = Car.objects.create(user=request.user)
        serializer = CarSerializer(car,data=request.data) 
        if serializer.is_valid():
            serializer.save()  
            return Response (({'success':True,'message':'Data Created','data':serializer.data}), status= status.HTTP_201_CREATED)  
        return Response (({'success':False,'message':'Something went wrong!!','data':serializer.errors}))     
    def put(self,request,pk, format=None):
        id = pk
        usercar = Car.objects.get(pk=id,user=request.user)   
        serializer = CarSerializer(usercar, data=request.data) 
        if serializer.is_valid():
            serializer.save()
            return Response (({'success':True,'message':'Data Updated','data':serializer.data}))
        return Response (({'success':False,'message':'Something went wrong!!','data':serializer.errors}))    
    def delete(self,request,pk):
        id = pk
        usercar = Car.objects.get(pk=id,user=request.user)  
        usercar.delete()
        return Response (({'success':True,'message':'Data Successfully Deleted'}))

# This is User AD ViewAPI
class AdAPI(APIView):
    premission_classes = (IsAuthenticated, )
    def get(self,request,pk=None,format=None):
        id = pk
        if id is not None:
            userads = Ad.objects.get(id=id)
            serializer = AdSerializer(userads) 
            return Response(({'success':True,'data':serializer.data}))
        userads = Ad.objects.filter(user=request.user) 
        serializer = AdSerializer(userads, many=True) 
        return Response(({'success':True,'data':serializer.data}))
    def post(self,request):
        ads = Ad.objects.create(user=request.user)
        serializer = AdSerializer(ads, data=request.data) 
        if serializer.is_valid():
            serializer.save()  
            return Response (({'success':True,'message':'Data Created','data':serializer.data}), status= status.HTTP_201_CREATED)  
        return Response (({'success':False,'message':'Something went wrong!!','data':serializer.errors}))
    def put(self,request,pk, format=None):
        id = pk
        ads = Ad.objects.get(pk=id,user=request.user) 
        serializer = AdSerializer(ads, data=request.data) 
        if serializer.is_valid():
            serializer.save()
            return Response (({'success':True,'message':'Data Updated','data':serializer.data}))
        return Response (({'success':False,'message':'Something went wrong!!','data':serializer.errors}))
    def delete(self, request,pk):
        id = pk
        usercar = Ad.objects.get(pk=id,user=request.user)
        usercar.delete()
        return Response (({'success':True,'message':'Data Successfully Deleted'}))