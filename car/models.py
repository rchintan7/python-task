from django.db import models
from django.contrib.auth.models import User


# This is User Adderss Model
class UserAddress(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)    #to=settings.AUTH_USER_MODEL
    no = models.CharField(max_length=100,null=True)
    street = models.CharField(max_length=100, null=True)
    city = models.CharField(max_length=100, null=True)
    country = models.CharField(max_length=100, null=True)
    
# This is User Car Model
class Car(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.ImageField(upload_to="images/car",null=True)
    brand = models.CharField(max_length=100, null=True)
    model = models.CharField(max_length=100, null=True)
    number_plate = models.CharField(max_length=13, null=True)
    
# This is User Ad Model
class Ad(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=300, null=True)
    description = models.TextField(null=True)
    price_per_km = models.IntegerField(null=True)
    date = models.DateField(auto_now_add=True,null=True, blank=True)
