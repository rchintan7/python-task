from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from django.contrib.auth.models import User
from car.models import *
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password
import json

# This is MyTokenObtainPair Serializer
class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        attrs = super().validate(attrs)
        data = {}
        return {
            "username": self.user.first_name +" "+ self.user.last_name,
            "email": self.user.email,
            "first_name": self.user.first_name,
            "last_name": self.user.last_name,
            **attrs,
        }
        
# This is Register Serializer
class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)
    class Meta:
        model = User
        fields = ('username', 'password', 'password2', 'email', 'first_name', 'last_name')
        extra_kwargs = {
            'first_name': {'required': True},
            'last_name': {'required': True}
        }
    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})
        return attrs
    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['email'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )
        user.set_password(validated_data['password'])
        user.save()
        email_obj = {
            'to': [validated_data['email']],
            'cc': [],
            'message': 'Your account is created successfully.\n\n Your login credentials are\nusername={}\npassword={}'.format(validated_data['username'], validated_data['password']),
            'subject': 'Registration Success'
        }
        # EmailManager.send_email(email_obj)
        return user  
    
# This is User Serializer
class UserSerializer(serializers.ModelSerializer):  
    class Meta:
        model = User
        fields = ("username","email","first_name","last_name")

# This is User Address Serializer
class UserAddressSerializer(serializers.ModelSerializer):   
    no = serializers.CharField(required=True)
    street = serializers.CharField(required=True)
    city = serializers.CharField(required=True)
    country = serializers.CharField(required=True) 
    class Meta:
        model = UserAddress
        fields = ("id","no","street","city","country")
        # read_only_fields = ('user')
        # fields = '__all__'
        
# This is Car Serializer
class CarSerializer(serializers.ModelSerializer):  
    brand = serializers.CharField(required=True)
    model = serializers.CharField(required=True)
    number_plate = serializers.CharField(required=True)  
    class Meta:
        model = Car
        fields = ('id','image','brand','model','number_plate')
        
# This is Ad Serializer
class AdSerializer(serializers.ModelSerializer):   
    title = serializers.CharField(required=True)
    description = serializers.CharField(required=True) 
    class Meta:
        model = Ad
        fields = ('id','title','description','price_per_km','date')